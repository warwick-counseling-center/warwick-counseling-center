import defaultTheme from 'tailwindcss/defaultTheme.js';

/** @type {import('tailwindcss').Config} config */
const config = {
  content: ['./index.php', './app/**/*.php', './resources/**/*.{php,vue,js}'],
  theme: {
    // OVERRIDE
    colors: {
      black: '#000000',
      current: 'currentColor',
      transparent: 'transparent',
      white: '#ffffff',
      green: {
        100: '#5D854F',
        200: '#426934',
        300: '#3F6432',
        400: '#2E4A24',
        500: '#1D3415',
      },
      orange: {
        100: '#FFFEF9',
        200: '#F8F6E9',
        300: '#F1EFDE',
        400: '#E2DAA4',
        500: '#DD8E17',
        600: '#CC8417',
        700: '#DE6A26',
        800: '#D15913',
      },
    },
    fontSize: {
      sm:   ['0.8125rem', '1.1538'], // 13px, 15px
      base: ['1rem', '1.5'],         // 16px, 24px
      md:   ['1.125rem', '1.6667'],  // 18px, 30px
      lg:   ['1.375rem', '1.4545'],  // 22px, 32px
      xl:   ['2rem', '1.1875'],      // 32px, 38px
      '2xl':  ['3rem', '1.1875'],      // 48px, 76px
      '3xl':  ['4rem', '1.1875'],      // 64px, 76px,
      '4xl':  ['4.25rem', '1.28125'],  // 64px, 28px,
    },
    container: {
      padding: {
        DEFAULT: '1rem',
        sm: '2rem',
      },
      center: true,
    },
    // EXTEND
    extend: {
      backgroundImage: {
        'challenges-and-conditions': "url('@images/blob-3.svg')",
      },
      colors: {},
      fontFamily: {
        sans: ['Proxima Nova', ...defaultTheme.fontFamily.sans],
        serif: ['"Caslon224Std"', ...defaultTheme.fontFamily.serif],
      },
      gridTemplateColumns: {
        nav: '1fr auto 1fr',
      },
    },
  },
  plugins: [
    'tailwindcss/forms',
  ],
};

export default config;
