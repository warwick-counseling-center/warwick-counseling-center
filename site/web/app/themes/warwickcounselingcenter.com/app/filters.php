<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Add <body> classes
 */
add_filter('body_class', function (array $classes) {
    /** Add page slug if it doesn't exist */
    if (is_single() || is_page() && !is_front_page()) {
        if (!in_array(basename(get_permalink()), $classes)) {
            $classes[] = basename(get_permalink());
        }
    }

    /** Bumps up base font size on medium devices and larger */
    $classes[] = 'md:text-md xl:text-lg';

    /** Clean up class names for custom templates */
    $classes = array_map(function ($class) {
        return preg_replace(['/-blade(-php)?$/', '/^page-template-views/'], '', $class);
    }, $classes);

    return array_filter($classes);
});

/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return sprintf(' &hellip; <a href="%s">%s</a>', get_permalink(), __('Continued', 'sage'));
});

/**
 * Disable Gutenberg editor
 */
add_filter('use_block_editor_for_post_type', function() {
    return false;
});

/**
 * Remove user contact methods
 */
add_filter('user_contactmethods', function($profile_fields) {
    unset($profile_fields['url']);
    unset($profile_fields['facebook']);
    unset($profile_fields['instagram']);
    unset($profile_fields['myspace']);
    unset($profile_fields['pinterest']);
    unset($profile_fields['soundcloud']);
    unset($profile_fields['tumblr']);
    unset($profile_fields['twitter']);
    unset($profile_fields['youtube']);
    unset($profile_fields['wikipedia']);

    return $profile_fields;
});

/**
 * Use full image with Simple Avatars plugin
 */
add_filter('pre_get_avatar_data', function($args) {
    $args['size'] = 'full';
    return $args;
}, 5 );

/**
 * Setup Google Maps API for ACF
 */
add_filter('acf/fields/google_map/api', function($args) {
    $args['key'] = 'AIzaSyBnWQFWED1w8Z0ALxiw3wi6S1rBB84W43w';
    return $args;
});
