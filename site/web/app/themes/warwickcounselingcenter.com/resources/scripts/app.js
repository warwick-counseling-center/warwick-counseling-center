import domReady from '@roots/sage/client/dom-ready';

/**
 * Application entrypoint
 */
domReady(async () => {
  const primaryNav = document.querySelector("[data-primary-navigation]");
  const mobileMenu = document.querySelector("[data-mobile-menu]");
  const mobileMenuToggle = document.querySelector("[data-mobile-menu-toggle]");
  const mobileMenuToggleIcons = document.querySelectorAll("[data-mobile-menu-toggle] svg");

  mobileMenuToggle.addEventListener('click', toggleMobileMenu);

  function toggleMobileMenu() {
    // Change primary nav bg color when mobile menu is active
    // if (primaryNav.classList.contains('bg-orange-100')) {
    //   primaryNav.classList.remove("bg-orange-100");
    //   primaryNav.classList.add("bg-white");
    // } else {
    //   primaryNav.classList.add("bg-orange-100");
    //   primaryNav.classList.remove("bg-white");
    // }
    // Toggle menu
    mobileMenu.classList.toggle('hidden');
    // Toggle icons
    mobileMenuToggleIcons.forEach(icon => {
      if (icon.classList.contains("block")) {
        icon.classList.remove("block");
        icon.classList.add("hidden");
      } else {
        icon.classList.add("block");
        icon.classList.remove("hidden");
      }
    });
    // Toggle aria attribute values on mobile menu toggle button
    if (mobileMenuToggle.getAttribute('aria-expanded') === 'true') {
      mobileMenuToggle.setAttribute('aria-expanded', 'false');
    } else {
      mobileMenuToggle.setAttribute('aria-expanded', 'true');
    }
  }
});

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
