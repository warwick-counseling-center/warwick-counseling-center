import domReady from '@roots/sage/client/dom-ready';
import { Loader as GoogleMapLoader } from '@googlemaps/js-api-loader';

/**
 * Application entrypoint
 */
domReady(async () => {
  const loader = new GoogleMapLoader ({
    apiKey: 'AIzaSyBnWQFWED1w8Z0ALxiw3wi6S1rBB84W43w',
    version: 'weekly',
    libraries: ['places']
  });

  const mapEl = document.getElementById("acf-google-map");

  const options = {
    scrollwheel: false,
    navigationControl: false,
    mapTypeControl: false,
    scaleControl: false,
    // draggable: false,
    // mapTypeId: 'roadmap',
    center: {
      lat: parseFloat(mapEl.dataset.lat) || 0,
      lng: parseFloat(mapEl.dataset.lng) || 0
    },
    zoom: parseInt(mapEl.dataset.zoom) || 16
  }

  loader
    .load()
    .then((google) => {
      // Map
      const map = new google.maps.Map(document.getElementById("acf-google-map"), options);
      // Marker
      new google.maps.Marker({
        position: options.center,
        map: map,
      });
    })
    .catch(e => {
      console.log(e);
    });
});

/**
 * @see {@link https://webpack.js.org/api/hot-module-replacement/}
 */
if (import.meta.webpackHot) import.meta.webpackHot.accept(console.error);
