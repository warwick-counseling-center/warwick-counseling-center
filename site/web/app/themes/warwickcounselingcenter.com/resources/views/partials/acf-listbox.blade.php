<div class="section xl:bg-challenges-and-conditions xl:bg-center xl:bg-no-repeat xl:bg-contain xl:py-24 xl:mx-8">
  <div class="container text-center xl:max-w-7xl">
    <div class="py-8 px-4 bg-orange-300 rounded-xl xl:px-8 xl:bg-transparent xl:rounded-none">
      <h2 class="mb-4 lg:mb-8 xl:mb-12">{{ the_sub_field('heading') }}</h2>
      <ul class="md:columns-3 lg:gap-8 xl:text-md">
        @while (have_rows('items')) @php(the_row())
          <li>{{ the_sub_field('text') }}</li>
        @endwhile
      </ul>
    </div>
  </div>
</div>
