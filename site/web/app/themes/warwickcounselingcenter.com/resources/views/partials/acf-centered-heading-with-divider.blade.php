<div class="section container text-center max-w-4xl">
  <h1 class="xl:text-4xl mb-8 lg:mb-16">{{ the_sub_field('heading') }}</h1>
  <img
      class="mx-auto w-16 lg:w-auto"
      src="@asset('images/diamonds-beige.svg')"
      width="124"
      role="presentation"
      aria-hidden="true">
</div>
