<div class="hero bg-orange-100 text-green-400 relative">
  <div class="container relative grid pt-4 md:grid-cols-2 md:pt-8 md:pb-16 md:gap-8 lg:grid-cols-5 lg:gap-16 xl:grid-cols-12 xl:gap-24">
    <div class="md:mt-5 lg:col-span-3 lg:mt-8 xl:mt-16 xl:col-span-7 xl:pr-16">
      <h1 class="mb-4 xl:text-3xl">{{ the_sub_field('heading') }}</h1>
      <p class="mb-4">{{ the_sub_field('description') }}</p>
      <p class="text-base mb-4 xl:mb-8">{{ the_sub_field('disclaimer') }}</p>
      <a class="btn btn-primary mb-4 md:mb-0" href="{{ the_sub_field('button_url') }}">{{ the_sub_field('button_text')}}</a>
    </div>
    <div class="hidden md:block lg:col-span-2 xl:col-span-5">
      @php
        $image = get_sub_field('image');
        $size = 'full';
        if ($image) {
          echo wp_get_attachment_image($image, $size, false, ["class" => "rounded-3xl"]);
        }
      @endphp
    </div>
    <img
      class="daffodil-beige"
      src="@asset('images/daffodil-beige-hero.svg')"
      width="80"
      role="presentation"
      aria-hidden="true">
  </div>
  <div class="arrow-beige">
    <a href="#skip-intro">
      <img
        src="@asset('images/arrow-beige.svg')"
        width="40"
        alt="skip intro arrow icon">
    </a>
  </div>
</div>
<div id="skip-intro"></div>
