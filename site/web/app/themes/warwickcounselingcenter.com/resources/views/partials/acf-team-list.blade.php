<div class="section">
  <div class="text-center">
    <img
      class="mx-auto w-16 mb-12 lg:mb-24 lg:w-auto xl:mb-36"
      src="@asset('images/diamonds-beige.svg')"
      width="124"
      role="presentation"
      aria-hidden="true">
    <h1 class="xl:text-4xl mb-8 lg:mb-24 xl:mb-32">{{ the_sub_field('heading') }}</h1>
  </div>
  @php
    $users = get_users([
      'fields' => 'all_with_meta',
      'role__in' => ['editor']
    ]);
    // Sort by custom sort_order field
    usort($users, function ( $a, $b ) { return ($a->sort_order > $b->sort_order) ? 1 : -1; });
  @endphp
  <div class="team-member container">
    @foreach ($users as $user)
      <div class="grid border-b-2 border-orange-300 pb-6 mb-12 lg:grid-cols-2 lg:gap-24 lg:pb-16 lg:pb-16 lg:mb-32 xl:gap-32 last:border-b-0 last:pb-0 last:mb-0">
        <div class="mt-8 mb-4 md:mb-8 lg:mt-0 xl:mb-16">
          <div class="img-style-border-top-right relative self-start z-0 w-3/4 mx-auto">
            @php echo get_avatar($user->ID, null, null, false, array(
              'scheme' => 'https',
              'class' => 'w-full h-auto rounded-3xl'
              )
            ) @endphp
          </div>
        </div>
        <div>
          <h2 class="mb-2 lg:mb-4">{{ esc_html($user->display_name) }}</h2>
          <div>
            @php echo wpautop($user->description,) @endphp
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
