<div class="page-header bg-orange-100 text-green-400">
  <div class="container text-center py-4 md:py-8 lg:py-24">
    <h1>{!! $title !!}</h1>
  </div>
</div>
