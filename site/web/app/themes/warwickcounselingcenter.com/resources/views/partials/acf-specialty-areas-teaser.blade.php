<div class="section container grid lg:grid-cols-2 lg:gap-8 xl:gap-16 items-center">
  <div class="relative mb-16 lg:mb-0 xl:pr-12">
    <h2 class="mb-2">{{ the_sub_field('heading') }}</h2>
    <p class="mb-4">{{ the_sub_field('paragraph') }}</p>
    <a href="{{ the_sub_field('cta_link') }}">{{ the_sub_field('cta_text') }}</a>
    {{-- <img class="daffodil-beige" src="@asset('images/daffodil-beige.svg')" width="80"> --}}
  </div>
  @if (have_rows('specialty_areas'))
  <div class="specialty-grid relative">
    <div class="specialty-grid-under grid grid-cols-6 grid-rows-2 justify-center absolute top-9 bottom-9 -left-4 -right-4 rounded-3xl border-2 border-solid border-orange-400">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
    <ul class="relative grid grid-cols-3 gap-4">
      @while (have_rows('specialty_areas')) @php(the_row())
      <li>
        <a class="btn btn-secondary" href="{{ the_sub_field('specialty_link') }}">
          {{ the_sub_field('specialty_text') }}
        </a>
      </li>
      @endwhile
    </ul>
  </div>
  @endif
</div>
