@php
  $id = get_the_ID();
@endphp

@if (have_rows('acf_flexible_page_content_blocks', $id))
  @while (have_rows('acf_flexible_page_content_blocks', $id)) @php the_row() @endphp
    @include('partials.'.get_row_layout())
  @endwhile
@else
  @if (is_page('contact'))
    @php $google_map = get_field('google_map', 'option'); @endphp
    @if ($google_map)
      <div class="section m-0 relative py-8 lg:py-16 xl:py-36">
        <div
          id="acf-google-map"
          class="absolute top-0 w-full h-full"
          data-zoom="@php echo esc_attr($google_map['zoom']) @endphp"
          data-lat="@php echo esc_attr($google_map['lat']) @endphp"
          data-lng="@php echo esc_attr($google_map['lng']) @endphp"></div>
        <div class="container text-center">
          <div class="p-4 pt-8 bg-orange-200 drop-shadow-md rounded-lg sm:rounded-xl sm:p-8 lg:drop-shadow-2xl lg:rounded-3xl lg:p-16 xl:p-24">
            @if (get_field('phone_number', 'option'))
            <h2 class="mb-8 mx-auto md:max-w-lg lg:mb-16 lg:max-w-2xl xl:mb-24 xl:text-2xl xl:max-w-4xl">
              Give us a call at
              <a href="tel:+1{{ the_field('phone_number', 'option') }}" target="_blank" rel="noreferrer">
                {{ the_field('phone_number', 'option') }}
              </a>
              to get the conversation started today.
            </h2>
            @elseif (get_field('email_address', 'option'))
            <h2 class="mb-8 mx-auto md:max-w-lg lg:mb-16 lg:max-w-2xl xl:mb-24 xl:text-2xl xl:max-w-4xl">
              Send us an email message to get the conversation started today.
            </h2>
            @endif
            <div class="grid md:grid-cols-2">
              <div class="md:order-last md:pl-8 lg:pl-16">
                {{-- <h3 class="xl:text-xl mb-2 xl:mb-4">Get directions</h3>
                <div>
                  <a href="{{ the_field('address_url', 'option') }}" target="_blank" rel="noreferrer">
                    {{ the_field('address', 'option') }}
                  </a>
                </div> --}}
                <h3 class="xl:text-xl mb-2 xl:mb-4">Office hours</h3>
                <div>
                  {{ the_field('office_hours', 'option') }}
                </div>
                <img
                  class="mx-auto py-8 md:py-16 xl:py-24"
                  src="@asset('images/stars-beige-2.svg')"
                  width="44">
              </div>
              <div class="border-t-2 border-orange-400 mt-8 pt-8 md:text-left md:mt-0 md:pt-0 md:border-t-0 md:border-r-2 md:pr-8 lg:pr-16">
                <h3 class="xl:text-xl mb-2 xl:mb-4">Send us an email</h3>
                <div class="contact-form">
                  {{ the_field('contact_form') }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endif
    @php the_content() @endphp
  @else
    @php the_content() @endphp
  @endif
@endif

@if (!is_front_page() && !is_page('contact'))
  @include('sections.footer-cta')
@endif
