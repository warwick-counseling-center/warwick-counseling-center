@php
  $has_design_accent = get_sub_field('has_design_accent');
@endphp
<div class="section container xl:max-w-6xl">
  @if (have_rows('repeat_heading_and_text_block', $id))
    @while (have_rows('repeat_heading_and_text_block', $id)) @php the_row() @endphp
      <div class="paragraph-list mb-8 lg:mb-16 xl:mb-24">
        <h2 class="mb-2 md:mb-4 @if ($has_design_accent) bg-repeat-x bg-[left_65%] lg:bg-[left_72%] xl:bg-[left_75%] has-accent xl:mb-8 xl:text-2xl @endif">
          @if ($has_design_accent)<span class="bg-orange-200 pr-4">@endif{{ the_sub_field('heading') }}@if ($has_design_accent)</span>@endif
        </h2>
        <div>{{ the_sub_field('text') }}</div>
      </div>
    @endwhile
  @endif
</div>
