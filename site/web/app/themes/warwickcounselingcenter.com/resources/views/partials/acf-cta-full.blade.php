<div class="section cta cta-full relative bg-orange-100 text-center mb-0">
  @include('sections.divider')
  <div class="container py-16 lg:py-32">
    <div class="max-w-2xl mx-auto">
      <h2 class="mb-2 xl:text-2xl">{{ the_sub_field('heading') }}</h2>
      <p>{{ the_sub_field('paragraph') }}</p>
    </div>
    <div class="w-3/4 max-w-3xl mx-auto">
      <div class="img-style-border-top-right relative z-0 mt-16 mb-8 lg:mt-24 lg:mb-16 xl:mt-32 xl:mb-24">
        <img
          class="daffodil-beige-left"
          src="@asset('images/daffodil-beige.svg')"
          width="80"
          role="presentation"
          aria-hidden="true">
        <img
          class="daffodil-beige-right"
          src="@asset('images/daffodil-beige.svg')"
          width="80"
          role="presentation"
          aria-hidden="true">
        @php
          $image = get_sub_field('image');
          $size = 'full';
          if ($image) {
            echo wp_get_attachment_image($image, $size, false, array('class' => 'w-full rounded-3xl'));
          }
        @endphp
      </div>
      <p class="mb-4">{{ the_sub_field('cta_text') }}</p>
      <a class="btn btn-primary" href="{{ the_sub_field('cta_button_link') }}">{{ the_sub_field('cta_button_text') }}</a>
    </div>
  </div>
</div>
