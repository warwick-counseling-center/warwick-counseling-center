<div class="section @if (get_sub_field('has_design_accent')) section-center-text-with-accent @endif relative">
  <div class="container text-center max-w-3xl xl:max-w-4xl">
    @if (get_sub_field('has_design_accent'))
      <img class="blob-left absolute left-0 -top-40 w-36 lg:w-64 xl:-top-64 -z-10" src="@asset('images/blob-1.svg')" width="309">
      <img class="blob-right absolute right-0 w-36 lg:w-64 lg:top-1/2 xl:top-2/3 -z-10" src="@asset('images/blob-2.svg')" width="285">
    @endif
    <h2 class="mb-2">{{ the_sub_field('heading') }}</h2>
    <p class="mb-4">{{ the_sub_field('paragraph') }}</p>
    <div><a href="{{ the_sub_field('link_url') }}">{{ the_sub_field('link_text') }}</a></div>
  </div>
</div>
