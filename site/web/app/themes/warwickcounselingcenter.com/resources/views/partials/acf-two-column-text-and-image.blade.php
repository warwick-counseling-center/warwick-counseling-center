<div class="section section-text-and-image container relative z-0 grid md:grid-cols-2 md:gap-8 lg:gap-16">
  <div class="
    @if(!get_sub_field('reverse_order'))
      img-style-border-bottom-right mb-24 md:mb-8
    @else
      img-style-border-top-right mt-24 md:mt-8 order-last
    @endif
    relative
    self-start
    z-0
    w-3/4
    mx-auto
  ">
    @php
      $image = get_sub_field('image');
      $size = 'full';
      if ($image) {
        echo wp_get_attachment_image($image, $size, false, array('class' => 'w-full rounded-3xl'));
      }
    @endphp
  </div>
  <div class="
    relative self-start md:pt-8 lg:pt-16 xl:pt-24
    @if(!get_sub_field('reverse_order')) xl:pl-12 @else xl:pr-12 @endif
  ">
    @if(get_sub_field('heading'))
    <h2 class="mb-2">{{ the_sub_field('heading') }}</h2>
    @endif
    @if(get_sub_field('paragraph'))
      <div class="mb-4">{{ the_sub_field('paragraph') }}</div>
    @endif
    @if(get_sub_field('cta_link') && get_sub_field('cta_text'))
      <a href="{{ the_sub_field('cta_link') }}">{{ the_sub_field('cta_text') }}</a>
    @endif
    <img class="daffodil-beige" src="@asset('images/daffodil-beige.svg')" width="80">
  </div>
</div>
