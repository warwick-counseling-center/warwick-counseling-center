<div class="section cta relative bg-orange-100 text-center mb-0">
  @include('sections.divider')
  <div class="container py-16 lg:py-32">
    <div class="max-w-2xl mx-auto">
      <h2 class="mb-2">{{ the_field('footer_cta_heading', 'option') }}</h2>
      <p class="mb-4 xl:mb-8">{{ the_field('footer_cta_paragraph', 'option') }}</p>
      <a class="btn btn-primary" href="{{ the_field('footer_cta_button_link', 'option') }}">
        {{ the_field('footer_cta_button_text', 'option') }}
      </a>
    </div>
  </div>
</div>
