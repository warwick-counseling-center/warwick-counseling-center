<footer class="footer bg-green-400 text-orange-200 py-8 lg:py-16">
  <div class="container grid lg:grid-cols-3 lg:gap-4 items-end">
    <div class="mb-8 lg:mb-0">
      <a href="{{ home_url('/') }}">
        <span class="sr-only">{{ bloginfo('name') }}</span>
        <img
          src="@asset('images/logo-and-icon-white.svg')"
          width="232"
          alt="{{ bloginfo('name') }} logo">
      </a>
    </div>
    {{-- <div class="mb-8 lg:mb-0">
      <div class="text-green-100 text-base xl:text-md font-bold uppercase mb-2">Address</div>
      <a href="{{ the_field('address_url', 'option') }}" target="_blank" rel="noreferrer">
        {{ the_field('address', 'option') }}
      </a>
    </div> --}}
    <div class="mb-8 lg:mb-0">
      @if (get_field('email_address', 'option'))
      <div>
        <div class="text-green-100 text-base xl:text-md font-bold uppercase mb-2 lg:hidden">Contact Information</div>
        <a href="mailto:{{ the_field('email_address', 'option') }}" target="_blank" rel="noreferrer">
          {{ the_field('email_address', 'option') }}
        </a>
      </div>
      @endif
      @if (get_field('phone_number', 'option'))
      <div>
        <a href="tel:+1{{ the_field('phone_number', 'option') }}" target="_blank" rel="noreferrer">
          {{ the_field('phone_number', 'option') }}
        </a>
      </div>
      @endif
    </div>
  </div>
  <div class="container grid md:grid-cols-2 md:items-center md:justify-between lg:pt-20">
    <div class="text-sm mb-4 md:mb-0 md:whitespace-nowrap">
      Copyright &copy; {{ date('Y') }} {{ bloginfo('name') }}. All rights reserved.
    </div>
    <div class="text-center md:text-right">
      <ul class="flex justify-center md:justify-end">
        @if (get_field('instagram_url', 'option'))
        <li class="flex-none">
            <a
              class="inline-block p-2 transition-opacity hover:opacity-75"
              href="{{ the_field('instagram_url', 'option') }}"
              title="WCC on Instagram"
            >
              <img src="@asset('images/icon-instagram.svg')" alt="Instagram icon" width="24" height="24">
            </a>
        </li>
        @endif
        @if (get_field('psychology_today_url', 'option'))
        <li class="flex-none">
            <a
              class="inline-block p-2 transition-opacity hover:opacity-75"
              href="{{ the_field('psychology_today_url', 'option') }}"
              title="WCC on Psychology Today"
            >
              <img src="@asset('images/icon-psychology-today.png')" alt="Psychology Today icon" width="24" height="24">
            </a>
        </li>
        @endif
      </ul>
    </div>
  </div>
</footer>
