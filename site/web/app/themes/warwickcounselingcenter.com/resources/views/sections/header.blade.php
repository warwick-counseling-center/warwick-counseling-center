<header class="bg-orange-100" data-primary-navigation>
  <div class="container py-4 sm:py-6 lg:py-8">
    <div class="grid grid-cols-nav items-center">
      <a class="w-5/6 lg:w-auto" href="{{ home_url('/') }}">
        <span class="sr-only">{{ bloginfo('name') }}</span>
        <img
          src="@asset('images/logo-green.svg')"
          width="333"
          alt="{{ bloginfo('name') }} logo">
      </a>
      <a href="{{ home_url('/') }}">
        <span class="sr-only">{{ bloginfo('name') }}</span>
        <img
          class="w-8 h-8 sm:w-12 sm:h-12 xl:w-20 xl:h-20"
          src="@asset('images/icon.png')"
          width="80"
          alt="{{ bloginfo('name') }} logo">
      </a>
      <div class="lg:flex ml-auto lg:items-baseline">
        <button class="lg:hidden rounded-md p-2 bg-orange-200 text-green-400 hover:text-green-500 focus:text-green-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-orange-400" type="button" data-mobile-menu-toggle aria-controls="mobile-menu" aria-expanded="false">
          <span class="sr-only">Open main menu</span>
          {{-- Heroicon name: outline/bars-3 --}}
          <svg class="block w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
          </svg>
          {{-- Heroicon name: outline/x-mark --}}
          <svg class="hidden h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
          </svg>
        </button>
        <div class="hidden lg:flex lg:items-baseline">
          @if (has_nav_menu('primary_navigation'))
            <nav aria-label="{{ wp_get_nav_menu_name('primary_navigation') }}" role="navigation">
              {!! wp_nav_menu([
                'theme_location' => 'primary_navigation',
                'menu_class' => 'menu flex',
                'echo' => false
              ]) !!}
            </nav>
          @endif
          <!-- Start SimplePractice Appointment-Request Widget Embed Code -->
          <a href="https://warwickcounselingcenter.clientsecure.me" class="spwidget-button btn btn-sm btn-tertiary lg:ml-4 xl:ml-8" data-spwidget-scope-id="373e6606-9716-403b-8d77-37118ac52b12" data-spwidget-scope-uri="warwickcounselingcenter" data-spwidget-application-id="7c72cb9f9a9b913654bb89d6c7b4e71a77911b30192051da35384b4d0c6d505b" data-spwidget-scope-global data-spwidget-autobind>Request Appointment</a>
          <script src="https://widget-cdn.simplepractice.com/assets/integration-1.0.js"></script>
          <!-- End SimplePractice Appointment-Request Widget Embed Code -->
        </div>
      </div>
    </div>
    {{-- Mobile menu --}}
    <div id="mobile-menu" class="mobile-menu pt-4 sm:pt-6 sm:pb-0 hidden lg:hidden" data-mobile-menu>
      @if (has_nav_menu('primary_navigation'))
        <nav aria-label="{{ wp_get_nav_menu_name('primary_navigation') }}" role="navigation">
          {!! wp_nav_menu([
            'theme_location' => 'primary_navigation',
            'menu_class' => 'menu',
            'echo' => false
          ]) !!}
        </nav>
      @endif
      <div class="grid grid-cols-1 mt-1 pt-4 border-t border-orange-400">
        <!-- Start SimplePractice Appointment-Request Widget Embed Code -->
        <a href="https://warwickcounselingcenter.clientsecure.me" class="spwidget-button btn btn-xs btn-tertiary" data-spwidget-scope-id="373e6606-9716-403b-8d77-37118ac52b12" data-spwidget-scope-uri="warwickcounselingcenter" data-spwidget-application-id="7c72cb9f9a9b913654bb89d6c7b4e71a77911b30192051da35384b4d0c6d505b" data-spwidget-scope-global data-spwidget-autobind>Request Appointment</a>
        <script src="https://widget-cdn.simplepractice.com/assets/integration-1.0.js"></script>
        <!-- End SimplePractice Appointment-Request Widget Embed Code -->
      </div>
      {{-- @endif --}}
    </div>
  </div>
</header>
